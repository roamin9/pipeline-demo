### Setup Jenkins

1. clone repo

``` shell
git clone https://gitlab.com/roamin9/pipeline-demo.git
cd pipeline-demo
```

2. running jenkins with docker

``` shell
docker run --rm --name myjenkins -p 8080:8080 -p 50000:50000 -v $(pwd)/jenkins_home:/var/jenkins_home jenkins/jenkins:lts
```

[Jenkins Image Document](https://github.com/jenkinsci/docker/blob/master/README.md)

3. Access http://localhost:8080

Set admin password and install plugins

Needed plugins:

	1. Git
	2. Pipeline
	3. Active Choice
	4. Blue Ocean

### Setup pipeline

1. Create a new pipeline as **test scenarios entry pipeline**

Use *Pipeline script from SCM*

repo: https://gitlab.com/roamin9/pipeline-demo.git

path: test_scenarios_entry/Jenkinsfile

2. Create other pipeline as **test scenario pipeline**

Use *Pipeline script from SCM*

repo: https://gitlab.com/roamin9/pipeline-demo.git

path: test_scenario_1/Jenkinsfile
